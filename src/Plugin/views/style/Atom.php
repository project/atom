<?php

namespace Drupal\atom\Plugin\views\style;

use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Default style plugin to render an RSS feed.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "atom",
 *   title = @Translation("Atom Feed"),
 *   help = @Translation("Generates an Atom feed from a view."),
 *   theme = "views_view_atom",
 *   display_types = {"feed"}
 * )
 */
class Atom extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  protected function defineOptions() {
    $options = parent::defineOptions();

    $options = [
      'subtitle' => ['default' => ''],
      'link_related' => ['default' => ''],
      'author_name' => ['default' => ''],
      'author_email' => ['default' => ''],
      'category' => ['default' => ''],
      'logo' => ['default' => ''],
      'icon' => ['default' => ''],
    ];

    return $options;
  }

  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['subtitle'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subtitle'),
      '#default_value' => $this->options['subtitle'],
      '#description' => $this->t('Contains a human-readable description or subtitle for the feed.'),
      '#maxlength' => 1024,
    ];

    $form['link_related'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description URL'),
      '#default_value' => $this->options['link_related'],
      '#description' => $this->t('URL to the description of this feed.'),
      '#maxlength' => 1024,
    ];

    $form['author_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Author name'),
      '#default_value' => $this->options['author_name'],
      '#description' => $this->t('Name of the author of this feed.'),
      '#maxlength' => 1024,
    ];

    $form['author_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Author email'),
      '#default_value' => $this->options['author_email'],
      '#description' => $this->t('Email of the author of this feed.'),
      '#maxlength' => 1024,
    ];

    $form['category'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Category'),
      '#default_value' => $this->options['category'],
      '#description' => $this->t('Specifies a category that the feed belongs to.'),
      '#maxlength' => 1024,
    ];

    $form['logo'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Logo'),
      '#default_value' => $this->options['logo'],
      '#description' => $this->t('Identifies a larger image which provides visual identification for the feed. Images should be twice as wide as they are tall.'),
      '#maxlength' => 1024,
    ];

    $form['icon'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Icon'),
      '#default_value' => $this->options['icon'],
      '#description' => $this->t('Identifies a small image which provides iconic visual identification for the feed. Icons should be square.'),
      '#maxlength' => 1024,
    ];
  }

  public function render() {
    if (empty($this->view->rowPlugin)) {
      trigger_error('Drupal\atom\Plugin\views\style\Atom: Missing row plugin', E_WARNING);
      return [];
    }
    $rows = [];

    foreach ($this->view->result as $row_index => $row) {
      $this->view->row_index = $row_index;
      $rows[] = $this->view->rowPlugin->render($row);
    }

    $build = [
      '#theme' => $this->themeFunctions(),
      '#view' => $this->view,
      '#options' => $this->options,
      '#rows' => $rows,
    ];
    unset($this->view->row_index);
    return $build;
  }

}
