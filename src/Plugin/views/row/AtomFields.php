<?php

namespace Drupal\atom\Plugin\views\row;

use Drupal\views\Plugin\views\row\RowPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Renders an Atom item based on fields.
 *
 * @ViewsRow(
 *   id = "atom_fields",
 *   title = @Translation("Atom fields"),
 *   help = @Translation("Display fields as Atom items."),
 *   theme = "views_view_row_atom",
 *   display_types = {"feed"}
 * )
 */
class AtomFields extends RowPluginBase {

  /**
   * Does the row plugin support to add fields to its output.
   *
   * @var bool
   */
  protected $usesFields = TRUE;

  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['title_field'] = ['default' => ''];
    $options['link_field'] = ['default' => ''];
    $options['date_field'] = ['default' => ''];
    $options['summary_field'] = ['default' => ''];
    $options['author_name_field'] = ['default' => ''];
    $options['author_email_field'] = ['default' => ''];
    $options['content_field'] = ['default' => ''];
    return $options;
  }

  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $initial_labels = ['' => $this->t('- None -')];
    $view_fields_labels = $this->displayHandler->getFieldLabels();
    $view_fields_labels = array_merge($initial_labels, $view_fields_labels);

    $form['title_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Title field'),
      '#description' => $this->t('The field that is going to be used as the Atom entry title for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['title_field'],
      '#required' => TRUE,
    ];
    $form['link_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Link field'),
      '#description' => $this->t('The field that is going to be used as the Atom entry link for each row. This must either be an internal unprocessed path like "node/123" or a processed, root-relative URL as produced by fields like "Link to content".'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['link_field'],
      '#required' => TRUE,
    ];
    $form['date_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Publication date field'),
      '#description' => $this->t('The field that is going to be used as the Atom entry pubDate for each row. It needs to be in RFC 2822 format.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['date_field'],
      '#required' => TRUE,
    ];
    $form['summary_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Summary field'),
      '#description' => $this->t('The field that is going to be used as the Atom entry summary for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['summary_field'],
      '#required' => TRUE,
    ];
    $form['author_name_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Author name field (Optional)'),
      '#description' => $this->t('The field that is going to be used as the Atom entry author name for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['author_name_field'],
      '#required' => FALSE,
    ];
    $form['author_email_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Author email field (Optional)'),
      '#description' => $this->t('The field that is going to be used as the Atom entry author email for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['author_name_email'],
      '#required' => FALSE,
    ];
    $form['content_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Content field (Optional)'),
      '#description' => $this->t('The field that is going to be used as the Atom entry content for each row.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['content_field'],
      '#required' => FALSE,
    ];
  }

  public function validate() {
    $errors = parent::validate();
    $required_options = ['title_field', 'link_field', 'date_field', 'summary_field'];
    foreach ($required_options as $required_option) {
      if (empty($this->options[$required_option])) {
        $errors[] = $this->t('Row style plugin requires specifying which views fields to use for Atom entry.');
        break;
      }
    }

    return $errors;
  }

  public function render($row) {
    static $row_index;
    if (!isset($row_index)) {
      $row_index = 0;
    }

    // Create the Atom item object.
    $item = new \stdClass();
    foreach (
      [
        'title' => 'title_field',
        'link' => 'link_field',
        'date' => 'date_field',
        'summary' => 'summary_field',
        'author_name' => 'author_name_field',
        'author_email' => 'author_email_field',
        'content' => 'content_field',
      ] as $name => $fieldname) {
      if (!empty($this->options[$fieldname])) {
        $field = $this->getField($row_index, $this->options[$fieldname]);
        $item->$name = is_array($field) ? $field : ['#markup' => $field];
      }
    }

    $row_index++;

    $build = [
      '#theme' => $this->themeFunctions(),
      '#view' => $this->view,
      '#options' => $this->options,
      '#row' => $item,
      '#field_alias' => isset($this->field_alias) ? $this->field_alias : '',
    ];

    return $build;
  }

  /**
   * Retrieves a views field value from the style plugin.
   *
   * @param $index
   *   The index count of the row as expected by views_plugin_style::getField().
   * @param $field_id
   *   The ID assigned to the required field in the display.
   *
   * @return string|null|\Drupal\Component\Render\MarkupInterface
   *   An empty string if there is no style plugin, or the field ID is empty.
   *   NULL if the field value is empty. If neither of these conditions apply,
   *   a MarkupInterface object containing the rendered field value.
   */
  public function getField($index, $field_id) {
    if (empty($this->view->style_plugin) || !is_object($this->view->style_plugin) || empty($field_id)) {
      return '';
    }
    return $this->view->style_plugin->getField($index, $field_id);
  }

}
